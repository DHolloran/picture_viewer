package com.holloran
{
    
    import com.holloran.events.ImageEvent;
    import com.holloran.loader.ImageLoader;
    import flash.display.Sprite;
    
    public class ImageViewer extends Sprite
    {
        private var _currentImage:int;
        
        private var _imageList:Array;
        
        private var _ld:ImageLoader;
        
        private var _path:String;
        
        public function ImageViewer ()
        {
            super();
            
            init();
        
        }
        
        public function display ():void
        {
            loadImg();
        
        }
        
        public function getCurrentImage ():int
        {
            return _currentImage;
        
        }
        
        public function next ():void
        {
            _currentImage++;
            
            if ( _currentImage == _imageList.length )
            {
                _currentImage = 0;
            }
            _ld = new ImageLoader(_path+_imageList[_currentImage]);
            _ld.addEventListener(ImageEvent.IMAGE_LOADED, onLoad);
        
        }
        
        public function set path (value:String):void
        {
            _path = value;
        
        }
        
        public function previous ():void
        {
            _currentImage--;
            
            if ( _currentImage < 0 )
            {
                _currentImage = _imageList.length - 1;
            }
            _ld = new ImageLoader(_path+_imageList[_currentImage]);
            _ld.addEventListener(ImageEvent.IMAGE_LOADED, onLoad);
        
        }
        
        public function setImageList (value:Array):void
        {
            _imageList = value;
        
        }
        
        private function init ():void
        {
            _imageList = [];
            _currentImage = 0
        
        }
        
        private function loadImg ():void
        {
            _ld = new ImageLoader(_path+_imageList[_currentImage]);
            _ld.addEventListener(ImageEvent.IMAGE_LOADED, onLoad);
        
        }
        
        private function onLoad (event:ImageEvent):void
        {
            if ( this.numChildren>0 )
            {
                this.removeChildAt(0);
            }
            this.addChild(event.image);
        
        }
    }
}