package
{
    
    import com.holloran.ImageViewer;
    import com.holloran.vo.ImageVO;
    import flash.display.Sprite;
    import flash.events.Event;
    import flash.events.MouseEvent;
    import flash.net.URLLoader;
    import flash.net.URLRequest;
    import flash.text.TextField;
    import libs.Controls;
    
    [SWF(width="800", height="600", backgroundColor="0x666666", frameRate="24" )]
    public class Main extends Sprite
    {
        
        private var _cLabel:TextField;
        
        private var _imgList:Array;
        
        private var _iv:ImageViewer;
        
        public function Main ()
        {
            super();
            
            init();
        
        }
        
        private function init ():void
        {
            //initiates viewer
            _iv = new ImageViewer();
            _iv.path = "assets/img/slideShow/";
            _iv.x = 70;
            _iv.y = 10;
            this.addChild(_iv);
            
            //initiates controls
            var controls:Controls = new Controls();
            controls.x = 50;
            controls.y = 225;
            this.addChild(controls);
            controls.next.addEventListener(MouseEvent.CLICK, onNext);
            controls.back.addEventListener(MouseEvent.CLICK, onBack);
            _cLabel = controls.tfLabel;
            
            //starts xml load process
            var urlLoader:URLLoader = new URLLoader();
            urlLoader.load(new URLRequest("assets/xml/images.xml"));
            urlLoader.addEventListener(Event.COMPLETE, onParse);
        
        }
        
        private function onBack (event:MouseEvent):void
        {
            _iv.previous();
            updateText();
        
        }
        
        private function onNext (event:MouseEvent):void
        {
            _iv.next();
            updateText();
        
        }
        
        private function onParse (e:Event):void
        {
            var iList:Array=[];
            _imgList=[];
            var _xmlData:XML = XML(e.target.data);
            
            for each ( var img:XML in _xmlData.picture )
            {
                var vo:ImageVO = new ImageVO();
                vo.name = img.imageName;
                vo.capt = img.caption;
                _imgList.push(vo);
                iList.push(vo.name);
            }
            _iv.setImageList(iList);
			_iv.display();
        
        }
        
        private function updateText ():void
        {
            var cImage:int = _iv.getCurrentImage();
            _cLabel.text = _imgList[cImage].capt;
        
        }
    }

}
